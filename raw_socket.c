#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <net/ethernet.h>
#include <linux/if_packet.h>
#include <linux/if.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <netinet/in.h>
#include <math.h>
#include "raw_socket.h"

unsigned int seq = 0;
#define DATA_SIZE 63


int ConexaoRawSocket(char *device) {
  int soquete;
  struct ifreq ir;
  struct sockaddr_ll endereco;
  struct packet_mreq mr;

  soquete = socket(AF_PACKET, SOCK_RAW, htons(ETH_P_ALL));  	/*cria socket*/
  if (soquete == -1) {
    printf("Erro no Socket\n");
    exit(-1);
  }

  memset(&ir, 0, sizeof(struct ifreq));  	/*dispositivo eth0*/
  memcpy(ir.ifr_name, device, sizeof(device));
  if (ioctl(soquete, SIOCGIFINDEX, &ir) == -1) {
    printf("Erro no ioctl\n");
    exit(-1);
  }
	

  memset(&endereco, 0, sizeof(endereco)); 	/*IP do dispositivo*/
  endereco.sll_family = AF_PACKET;
  endereco.sll_protocol = htons(ETH_P_ALL);
  endereco.sll_ifindex = ir.ifr_ifindex;
  if (bind(soquete, (struct sockaddr *)&endereco, sizeof(endereco)) == -1) {
    printf("Erro no bind\n");
    exit(-1);
  }


  memset(&mr, 0, sizeof(mr));          /*Modo Promiscuo*/
  mr.mr_ifindex = ir.ifr_ifindex;
  mr.mr_type = PACKET_MR_PROMISC;
  if (setsockopt(soquete, SOL_PACKET, PACKET_ADD_MEMBERSHIP, &mr, sizeof(mr)) == -1)	{
    printf("Erro ao fazer setsockopt\n");
    exit(-1);
  }

  return soquete;
}

void serialize_msg(const msg_t* msg, unsigned char* buffer) {
    memcpy(buffer, msg, sizeof(msg_t));
}

void deserialize_msg(const unsigned char* buffer, msg_t* msg) {
    memcpy(msg, buffer, sizeof(msg_t));
}

msg_t create_message(int tam, int type, unsigned char* data) {
    msg_t message;
    message.init_marker = 0x7E;  // Correspondente aos bits 01111110
    message.tam = tam;
    message.seq = seq;
    seq = ++seq % DATA_SIZE;
    message.type = type;
    strncpy(message.data, data, tam);
    int parity_bytes = 6+6+4+63;
    unsigned char buffer[parity_bytes];
    memcpy(buffer, &message + 1, parity_bytes);
    message.parity = crc(buffer, parity_bytes);  // Correspondente aos bits 10101010
    return message;
}

/*void send_file(FILE *arq, int socket) {
  long int size, msg_count;
  double teto;
  msg_t *msg_array;
  fseek(arq, 0L, SEEK_END);
  size = ftell(arq);
  teto = (double)size/DATA_SIZE;
  msg_count = ceil(teto);
  msg_array = malloc(msg_count * sizeof(msg_t));
  rewind(arq);
  
  for(int i = 0; i < msg_count-1; i++) {
    unsigned char buffer[DATA_SIZE];
    fread(buffer, sizeof(unsigned char), 63, arq);
    msg_array[i] = create_message(DATA_SIZE, 8, buffer);
    size -= DATA_SIZE;
  }
  unsigned char buffer[DATA_SIZE];
  fread(buffer, size, 1, arq);
  msg_array[msg_count-1] = create_message(size, 8, buffer);

  unsigned char msg_buffer[sizeof(msg_t)];
  for(int i = 0; i < msg_count; i++) {
    serialize_msg(&msg_array[i], msg_buffer);
    if (send(socket, msg_buffer, sizeof(msg_t), 0) == -1) {
        perror("Erro ao enviar mensagem");
        exit(EXIT_FAILURE);
    }
  }
}*/
void send_file(FILE *arq, int socket) {
  long int size;
  fseek(arq, 0L, SEEK_END);
  size = ftell(arq);
  rewind(arq);
  while(size != 0) {
  	msg_t msg;
  	if(size < DATA_SIZE) {
  		unsigned char buffer[size];
		fread(buffer, sizeof(unsigned char), size, arq);
		msg = create_message(size, 8, buffer);
		unsigned char msg_buffer[sizeof(msg_t)];
		serialize_msg(&msg, msg_buffer);
		if (send(socket, msg_buffer, sizeof(msg_t), 0) == -1) {
		    perror("Erro ao enviar mensagem");
		    exit(EXIT_FAILURE);
		}
		size = 0;
  	}
  	else {
	  	unsigned char buffer[DATA_SIZE];
		fread(buffer, sizeof(unsigned char), DATA_SIZE, arq);
		msg = create_message(DATA_SIZE, 8, buffer);
		unsigned char msg_buffer[sizeof(msg_t)];
		serialize_msg(&msg, msg_buffer);
		if (send(socket, msg_buffer, sizeof(msg_t), 0) == -1) {
		    perror("Erro ao enviar mensagem");
		    exit(EXIT_FAILURE);
		}
		size -= DATA_SIZE;
	}
  }
}

uint8_t crc(uint8_t *data, size_t len) {
    uint8_t crc = 0xff;
    size_t i, j;
    for (i = 0; i < len; i++) {
        crc ^= data[i];
        for (j = 0; j < 8; j++) {
            if ((crc & 0x80) != 0)
                crc = (uint8_t)((crc << 1) ^ 0x31);
            else
                crc <<= 1;
        }
    }
    return crc;
}
