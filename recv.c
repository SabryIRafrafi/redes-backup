#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <net/if.h>
#include <netinet/in.h>
#include <linux/if_packet.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <net/ethernet.h>
#include <dirent.h>
#include <errno.h>
#include "raw_socket.h"

#define BUFFER_SIZE sizeof(msg_t)

int main() {
    int socket_desc = ConexaoRawSocket("eno1");

    unsigned char buffer[BUFFER_SIZE];
    ssize_t num_bytes;
    FILE *arq;
    DIR *dir;

    while (1) {
        num_bytes = recv(socket_desc, buffer, BUFFER_SIZE, 0);
        if (num_bytes == -1) {
            perror("Erro ao receber mensagem");
            exit(EXIT_FAILURE);
        }

        msg_t recv_msg;
        deserialize_msg(buffer, &recv_msg);
        if (recv_msg.init_marker == 0x7E) {
            printf("Mensagem recebida\n");
            printf("Init Marker: %02X\n", recv_msg.init_marker);
            printf("Tam: %d\n", recv_msg.tam);
            printf("Seq: %d\n", recv_msg.seq);
            printf("Type: %d\n", recv_msg.type);
            printf("Data: %s\n", recv_msg.data);
            printf("Parity: %02X\n", recv_msg.parity);
            
            int parity_size = 6+6+4+63;
			unsigned char buffer[parity_size];
			memcpy(buffer, &recv_msg + 1, parity_size);
			uint8_t parity = crc(buffer, parity_size);
			printf("%02X\n", parity);
            //if(recv_msg.parity != parity)
            	 //printf("nack\n");
            switch (recv_msg.type)
            {
            case 0:
                arq = fopen(recv_msg.data, "w");
                break;
			case 2:
				FILE *arq = fopen(recv_msg.data, "r");
            	send_file(arq, socket_desc);
            	msg_t message2 = create_message(0, 9, "");
		        printf("Mensagem enviada\n");
		        printf("Init Marker: %02X\n", message2.init_marker);
		        printf("Tam: %d\n", message2.tam);
		        printf("Seq: %d\n", message2.seq);
		        printf("Type: %d\n", message2.type);
		        printf("Data: %s\n", message2.data);
		        printf("Parity: %02X\n", message2.parity);
		        serialize_msg(&message2, buffer);
		        if (send(socket_desc, buffer, BUFFER_SIZE, 0) == -1) {
		            perror("Erro ao enviar mensagem");
		            close(socket_desc);
		            exit(EXIT_FAILURE);
		        }
		        break;
		        
            case 4:
                chdir(recv_msg.data);
                printf("Conteúdo de %s:\n", getcwd(NULL, (size_t) NULL));
                dir = opendir(getcwd(NULL, (size_t) NULL));
                for (struct dirent *entry = readdir(dir); entry; entry = readdir(dir)) {
                    printf("    %s:  ", entry->d_name);
                    switch (entry->d_type)
                    {
                        case DT_UNKNOWN:
                            printf ("(desconhecido)\n") ;
                            break ;
                        case DT_REG:
                            printf ("(arquivo)\n") ;
                            break ;
                        case DT_DIR:
                            printf ("(diretorio)\n") ;
                            break ;
                        default:
                            printf ("(outros)\n") ;
                    }
                }
                closedir(dir);
                break;

            case 8:
                fwrite(recv_msg.data, 1, recv_msg.tam, arq);
                printf("escreveu\n");
                break;
            
            case 9:
                fclose(arq);
                break;

            default:
                break;
            }
        }
    }
    close(socket_desc);
    return 0;
}
