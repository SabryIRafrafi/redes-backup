#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <net/if.h>
#include <netinet/in.h>
#include <linux/if_packet.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <net/ethernet.h>
#include "raw_socket.h"

#define BUFFER_SIZE sizeof(msg_t)

int main() {
    int socket_desc = ConexaoRawSocket("lo");

    /*struct sockaddr_ll dest_addr;
    memset(&dest_addr, 0, sizeof(struct sockaddr_ll));
    dest_addr.sll_family = AF_PACKET;
    dest_addr.sll_protocol = htons(ETH_P_ALL);
    dest_addr.sll_ifindex = if_nametoindex("lo");*/

    while (1) {
        unsigned char command[64];
        
        scanf("%s", command);
        if (strncmp(command, "bac", 3) == 0) {
            unsigned char file_name[64];
            scanf("%s", file_name);
            msg_t message = create_message(strlen(file_name)+1, 0, file_name);

            unsigned char buffer[BUFFER_SIZE];
            serialize_msg(&message, buffer);
            if (send(socket_desc, buffer, BUFFER_SIZE, 0) == -1) {
                perror("Erro ao enviar mensagem");
                close(socket_desc);
                exit(EXIT_FAILURE);
            }
            printf("Mensagem enviada\n");
            printf("Init Marker: %02X\n", message.init_marker);
            printf("Tam: %d\n", message.tam);
            printf("Seq: %d\n", message.seq);
            printf("Type: %d\n", message.type);
            printf("Data: %s\n", message.data);
            printf("Parity: %02X\n", message.parity);
            FILE *arq = fopen(file_name, "r");
            send_file(arq, socket_desc);

            msg_t message2 = create_message(0, 9, "");
            printf("Mensagem enviada\n");
            printf("Init Marker: %02X\n", message2.init_marker);
            printf("Tam: %d\n", message2.tam);
            printf("Seq: %d\n", message2.seq);
            printf("Type: %d\n", message2.type);
            printf("Data: %s\n", message2.data);
            printf("Parity: %02X\n", message2.parity);
            serialize_msg(&message2, buffer);
            if (send(socket_desc, buffer, BUFFER_SIZE, 0) == -1) {
                perror("Erro ao enviar mensagem");
                close(socket_desc);
                exit(EXIT_FAILURE);
            }
        }
        else if (strncmp(command, "cd", 2) == 0) {
            unsigned char path[64];
            scanf("%s", path);
            msg_t message = create_message(strlen(path)+1, 4, path);

            unsigned char buffer[BUFFER_SIZE];
            serialize_msg(&message, buffer);
            if (send(socket_desc, buffer, BUFFER_SIZE, 0) == -1) {
                perror("Erro ao enviar mensagem");
                close(socket_desc);
                exit(EXIT_FAILURE);
            }
        }
        else if (strncmp(command, "rec", 3) == 0) {
        	unsigned char file_name[64];
            scanf("%s", file_name);
            msg_t message = create_message(strlen(file_name)+1, 2, file_name);

            unsigned char buffer[BUFFER_SIZE];
            serialize_msg(&message, buffer);
            if (send(socket_desc, buffer, BUFFER_SIZE, 0) == -1) {
                perror("Erro ao enviar mensagem");
                close(socket_desc);
                exit(EXIT_FAILURE);
            }
            int file_recieved = 0;
             while (!file_recieved) {
				int num_bytes = recv(socket_desc, buffer, BUFFER_SIZE, 0);
				if (num_bytes == -1) {
				    perror("Erro ao receber mensagem");
				    exit(EXIT_FAILURE);
				}

				msg_t recv_msg;
				deserialize_msg(buffer, &recv_msg);
				if (recv_msg.init_marker == 0x7E) {
				    FILE *arq;
				
				    printf("Mensagem recebida\n");
				    printf("Init Marker: %02X\n", recv_msg.init_marker);
				    printf("Tam: %d\n", recv_msg.tam);
				    printf("Seq: %d\n", recv_msg.seq);
				    printf("Type: %d\n", recv_msg.type);
				    printf("Data: %s\n", recv_msg.data);
				    printf("Parity: %02X\n", recv_msg.parity);
				    
				    int parity_size = 6+6+4+63;
					unsigned char buffer[parity_size];
					memcpy(buffer, &recv_msg + 1, parity_size);
					uint8_t parity = crc(buffer, parity_size);
					printf("%02X\n", parity);
				    //if(recv_msg.parity != parity)
				    	 //printf("nack\n");
				    switch (recv_msg.type)
				    {
				    case 0:
				        arq = fopen(recv_msg.data, "w");
				        break;

				    case 8:
				        fwrite(recv_msg.data, 1, recv_msg.tam, arq);
				        printf("escreveu\n");
				        break;
				    
				    case 9:
				        fclose(arq);
				    	file_recieved = 1;
				        break;

				    default:
				        break;
				    }
				}
			}
        }
    }

    /*unsigned char path[64];
    sprintf(path, "%s", "/home/sabryir/UFPR/2023.1");
    msg_t message = create_message(strlen(path), 4, path);

    unsigned char buffer[BUFFER_SIZE];
    serialize_msg(&message, buffer);
    if (send(socket_desc, buffer, BUFFER_SIZE, 0) == -1) {
        perror("Erro ao enviar mensagem");
        close(socket_desc);
        exit(EXIT_FAILURE);
    }*/

    /*printf("Mensagem enviada\n");
    printf("Init Marker: %02X\n", message.init_marker);
    printf("Tam: %d\n", message.tam);
    printf("Seq: %d\n", message.seq);
    printf("Type: %d\n", message.type);
    printf("Data: %s\n", message.data);
    printf("Parity: %02X\n", message.parity);*/

    /*FILE *arq = fopen("teste.txt", "r");
    send_file(arq, socket_desc);

    message = create_message(10, 9, "teste2.txt");
    serialize_msg(&message, buffer);
    if (send(socket_desc, buffer, BUFFER_SIZE, 0) == -1) {
        perror("Erro ao enviar mensagem");
        close(socket_desc);
        exit(EXIT_FAILURE);
    }*/

    close(socket_desc);
    return 0;
}
