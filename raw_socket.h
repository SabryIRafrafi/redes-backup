#ifndef __RAW_SOCKET__
#define __RAW_SOCKET__

typedef struct {
    char init_marker;             // 8 bits 
    unsigned int tam : 6;         // 6 bits
    unsigned int seq : 6;         // 6 bits
    unsigned int type : 4;        // 4 bits
    unsigned char data[63];       // 0 - 63 bytes
    uint8_t parity;               // 8 bits 
} msg_t;

void send_file(FILE *arq, int socket);

msg_t create_message(int tam, int type, unsigned char* data);

int ConexaoRawSocket(char *device);

void serialize_msg(const msg_t* msg, unsigned char* buffer);

void deserialize_msg(const unsigned char* buffer, msg_t* msg);

uint8_t crc(uint8_t *data, size_t len);

#endif
