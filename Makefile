all: send recv
recv:
	gcc -g recv.c raw_socket.c -lm -lnsl -o recv
send:
	gcc -g send.c raw_socket.c -lm -lnsl -o send
purge:
	rm recv send